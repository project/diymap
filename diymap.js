$(document).ready(function() {

	diymap_init();

});

function diymap_renum(type) {
  cur_id = 0;
  $('table#' + type + '_fieldset tbody').children('tr').each( function() {
    $(this).children('td').each( function() {
      $(this).find('*').each(function() {
        if($(this).attr('id')) {    					        	  		
          $(this).attr('id', $(this).attr('id').replace(/\d+/, cur_id));
        }
        if($(this).attr('name')) {
          $(this).attr('name', $(this).attr('name').replace(/\d+/, cur_id));
        }    			
   	  });
    });
  	cur_id++;
  });
}

function diymap_init() {
	
  // Farbtastic colorpickers
  $('input.color').each(function () {      
    $(this).parent().next('.colorpicker').farbtastic("#" + $(this).attr('id'));
  });
  $('.colorpicker').hide();
  $('input.color').click( function() {
    $('.colorpicker').hide();      
    $(this).parent().next('.colorpicker').slideDown('fast');
  });

  // Initialize delete buttons
  diymap_init_del('range');
  diymap_init_del('state');
  diymap_init_del('region');
  diymap_init_del('point');
  
  // Initialize add buttons
  diymap_init_add('range');
  diymap_init_add('state');
  diymap_init_add('region');
  diymap_init_add('point');
  
}

function diymap_init_add(type) {
	
    $('#add_more_' + type + '_button').click(function() {
        
        // Add the new row     
        new_item = $('table#' + type + '_fieldset tbody tr:first').clone().get(0);
        $(new_item).attr('id', $(this).attr('id').replace(/\d+/, '0000'));
        $(new_item).appendTo('table#' + type + '_fieldset tbody');
        diymap_renum(type);
        
        // Re-affect removal checkbox
        $('table#' + type + '_fieldset tbody tr:last input.del_' + type).click(function() {
          if($(this).parents("tbody").children().size() > 1) {
             $(this).parents("tr").remove();
             diymap_renum(type);
          }
        });
        
        if(type == 'range') {
	        // Re-affect colorpicker
	        colorpicker = $('table#range_fieldset tbody tr:last .colorpicker').get(0);
	        colorfield  = $('table#range_fieldset tbody tr:last input.color').get(0);
	        $(colorpicker).farbtastic($(colorfield));      
	        $(colorfield).click( function() {
	      	 $('.colorpicker').hide();      
	      	 $(this).parent().next('.colorpicker').slideDown('fast');
	        });
        }
        
        return false;
        
    });
        
}

function diymap_init_del(type) {
  $('input.del_' + type).click(function() {
    if($(this).parents("tbody").children().size() > 1) {
       $(this).parents("tr").remove();
       diymap_renum(type);         
    }                  
  });
}